document.getElementById("main__modal__header-closemodal").addEventListener("click", closeMenu);
function closeMenu() {
    document.getElementById("main__modal").classList.toggle("hide");
    document.getElementById("main").classList.toggle("opacity");
    hideList();
    document.getElementById("main__modal__menu-input-p").classList.add("hide");
    document.getElementById("main__modal__button").classList.add("hide");
}

document.getElementById("main__header__button").addEventListener("click", openMenu);
function openMenu() {
    document.getElementById("main__modal").classList.toggle("hide");
    document.getElementById("main__modal").classList.add("animated");
    document.getElementById("main__modal").classList.add("bounceInDown");
    document.getElementById("main").classList.toggle("opacity")
}

function hideList(){
    let a = document.getElementsByClassName("main__modal__menu-input");
    for (let i=0; i<a.length; i++){
        a[i].classList.add("hide")
    }
    let b = document.getElementsByClassName("main__modal__menu-item");
    for (let j=0; j<b.length; j++){
        b[j].classList.remove("active")
    }
}

document.getElementById("cardio").addEventListener("click", openCardioList);

function openCardioList() {
    hideList();
    document.getElementById("cardio").classList.toggle("active");
    document.getElementById("main__modal__menu-input-p").classList.remove("hide");
    document.getElementById("main__modal__button").classList.remove("hide");
        let a = document.getElementsByClassName("cardio");
        for (let i=0; i<a.length; i++){
            a[i].classList.toggle("hide")
        }
}

document.getElementById("dent").addEventListener("click", openDentList);
function openDentList() {
    hideList();
    document.getElementById("dent").classList.toggle("active");
    document.getElementById("main__modal__menu-input-p").classList.remove("hide");
    document.getElementById("main__modal__button").classList.remove("hide");
    let a = document.getElementsByClassName("dent");
    for (let i=0; i<a.length; i++){
        a[i].classList.toggle("hide")
    }
}

document.getElementById("terapevt").addEventListener("click", openTerapevtList);

function openTerapevtList() {
    hideList();
    document.getElementById("terapevt").classList.toggle("active");
    document.getElementById("main__modal__menu-input-p").classList.remove("hide");
    document.getElementById("main__modal__button").classList.remove("hide");
    let a = document.getElementsByClassName("terapevt");
    for (let i=0; i<a.length; i++){
        a[i].classList.toggle("hide")
    }
}

